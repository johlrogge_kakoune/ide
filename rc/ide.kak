echo -debug %val{source}/../

declare-option -hidden str ide_kak_file "%val{source}" 

define-command -override -docstring "open IDE windows" ide %{evaluate-commands %sh{
       source  $(dirname $kak_opt_ide_kak_file)/kak_ide_open_windows
}}
