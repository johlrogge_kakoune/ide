Describe "Kakoune extension scripts"
  Describe "IDE"
  	Set 'errexit:off'
        client_main() {
            %text
    	    #|set global jumpclient main
    	}
    	client_tools() {
            %text
            #|new rename-client tools
            #|set global toolsclient tools
    	}
    	client_docs() {
            %text
            #|new rename-client docs
            #|set global docsclient docs
        }
	Context "no windows are open"
		kak_client_list=""
    		It "opens all windows"
      			When run source ./rc/kak_ide_open_windows
      			The output should include "$(client_main)"
      			The output should include "$(client_tools)"
      			The output should include "$(client_docs)"
      		End
      	End
      	Context "tools window is open"
      		kak_client_list="tools"
      		It "opens main and docs windows"
      			When run source ./rc/kak_ide_open_windows
      			The output should include "$(client_main)"
      			The output should not include "$(client_tools)"
      			The output should include "$(client_docs)"
      		End
      	End
      	Context "docs window is open"
      		kak_client_list="docs"
      		It "opens main and tools windows"
      			When run source ./rc/kak_ide_open_windows
      			The output should include "$(client_main)"
      			The output should include "$(client_tools)"
      			The output should not include "$(client_docs)"
      		End
      	End
      	Context "docs and tools window is open"
      		kak_client_list="docs tools"
      		It "opens main window"
      			When run source ./rc/kak_ide_open_windows
      			The output should include "$(client_main)"
      			The output should not include "$(client_tools)"
      			The output should not include "$(client_docs)"
      		End
      	End
      	Context "main and docs window is open"
      		kak_client_list="main docs"
      		It "opens tools window"
      			When run source ./rc/kak_ide_open_windows
      			The output should not include "$(client_main)"
      			The output should include "$(client_tools)"
      			The output should not include "$(client_docs)"
      		End
      	End
      	Context "main and tools window is open"
      		kak_client_list="main tools"
      		It "opens docs window"
      			When run source ./rc/kak_ide_open_windows
      			The output should not include "$(client_main)"
      			The output should not include "$(client_tools)"
      			The output should include "$(client_docs)"
      		End
      	End
    End
End
